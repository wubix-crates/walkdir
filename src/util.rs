use librust::io;
use librust::path::{Path, PathExt};

pub async fn device_num<P: AsRef<Path>>(path: P) -> io::Result<u64> {
    path.as_ref().metadata().await.map(|md| md.dev())
}
